> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Web Applications Development

## Daniel Rigby

### Assignment #4 Requirements:

1. Create a MVC
2. Model: - The “business” layer. how do we represent the data? Defines business objects, and provides
	methods for business processing (e.g., Customer and CustomerDB classes).
3. Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from
	requests, updates the model, saves data to the data store, and forwards updated model to JSPs for
	presentation (e.g., CustomerListServlet).
4. View: How do I render the result? Defines the presentation—using .html or .jsp files (e.g., index.html,
	index.jsp, thanks.jsp)


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A4;
* Screenshot of form;
* Screenshot of completed form
* Screenshot of submission



#### Assignment Screenshots:

*Screenshot of form:*

![form](img/form.PNG)

*Screenshot of data:*

![data](img/data.PNG)

*Screenshot of thanks:*

![thanks](img/thanks.PNG)




