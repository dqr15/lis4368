> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Daniel Rigby

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Create and run Hello.java
	- Setup Bitbucket

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Download and install Mysql
	- Develop servlet
	- Deploy servlet
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create petstore DB
	- Create petstore ERD
	- Successfully forward engineer database
	
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create MVC
	- Validate server side testing 
	- Successfully submit user information
	
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Create appropriate files
	- Validate database side testing 
	- Successfully submit user information
	
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Successfully clone student files
	- Modify Meta tags
	- Provide form validation
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- MVC framework, using basic client-, server-side validation
	- Prepared statements to help prevent SQL injection.
	- JSTL to prevent XSS. Also, completes CRUD functionality.