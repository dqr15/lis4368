<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Tue, 02-21-17, 21:58:59 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Daniel Rigby">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Project1 </title>

	<%@ include file="/css/include_css.jsp" %>		

</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<h2>Customers</h2>
					
					<form id="add_customer_form" method="post" class="form-horizontal" action="#">

						<div class="form-group">
							<label class="col-sm-3 control-label">First Name:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="fname" maxlength="15" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Last Name:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="lname" maxlength="30" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Email:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" maxlength="100"/>
							</div>
						</div>
						<div class="form-group">
						        <label class="col-sm-3 control-label">Street:</label>
							<div class="col-sm-5">
							     <input type="text" class="form-control" name="street" maxlength="30"/>
							</div>     
						</div>
						<div class="form-group">
						        <label class="col-sm-3 control-label">City:</label>
							<div class="col-sm-5">
							     <input type="text" class="form-control" name="city" maxlength="30"/>
							</div>  
						</div>  
						
						<div class="form-group">
						        <label class="col-sm-3 control-label">State:</label>
							<div class="col-sm-5">
							    <input type="text" class="form-control" name="state" maxlength="2"/>
							</div>
						</div>
						<div class="form-group">
						       <label class="col-sm-3 control-label">Zip:</label>
						       <div class="col-sm-5">
							    <input type="text" class="form-control" name="zipcode" maxlength="9"/>
						       </div>	    
						</div>
						<div class="form-group">
						       <label class="col-sm-3 control-label">Phone:</label>
						       <div class="col-sm-5">
							   <input type="text" class="form-control" name="phone" maxlength="10"/>
						       </div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Balance:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="balance" maxlength="7"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Sales:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="sales" maxlength="7"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Notes:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="notes" maxlength="100"/>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-5 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		
 
<script type="text/javascript">
$(document).ready(function() {

	$('#add_customer_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {

				fname: {
							validators: {
									notEmpty: {
											message: 'First name required'
									},
									stringLength: {
											min: 1,
											max: 15,
											message: 'First name no more than 15 characters'
									},
									regexp: {
										//http://www.regular-expressions.info/
										//http://www.regular-expressions.info/quickstart.html
										//http://www.regular-expressions.info/shorthand.html
										//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
										//alphanumeric (also, "+" prevents empty strings):
										regexp: /^[a-zA-Z\-]+$/,
										message: 'First name can only contain letters and hyphens'
									},									
							},
					},

				lname: {
							validators: {
									notEmpty: {
											message: 'Last name required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Last name no more than 30 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z\-]+$/,
										message: 'Last name can only contain letters and hyphens'
									},									
							},
					},
									
					email: {
							validators: {
									notEmpty: {
											message: 'Email address is required'
									},
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
									regexp: /^([A-Za-z0-9_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})$/,
										message: 'Must include valid email'
									},											
							},
					},
					street: {
							validators: {
									notEmpty: {
											message: 'street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'max 30 characters'
									},
									regexp: {
										//alphanumeric (also, "+" prevents empty strings):
										regexp: /^([A-Za-z0-9\-\s\.]+$/,
										message: 'may contain only letters, numbers, periods, commas and white spaces'
									},									
							},
					},
					city: {
							validators: {
									notEmpty: {
											message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'max 30 characters'
									},
									regexp: {
										regexp: /^([A-Za-z0-9\,\. _]*[A-Za-z0-9][A-Za-z0-9\,\. _])*$/,
										message: 'can contain only letters and numbers'
									},									
							},
					},
					state: {
							validators: {
									notEmpty: {
											message: 'State required'
									},
									stringLength: {
											min: 1,
											max: 2,
											message: 'max 2 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z\-]+$/,
										message: 'in "FL" format. Only letters'
									},									
							},
					},
					zipcode: {
						validators: {
									notEmpty: {
											message: 'zipcode required'
									},
									stringLength: {
											min: 5,
											max: 9,
											message: 'min 5 digits, max 9 digits'
									},
									regexp: {
										regexp: /^([0-9])+$/,
										message: 'only numbers accepted'
									},									
							},
					},
					phone: {
						validators: {
							notEmpty: {
								message: 'phone number required'
							},
							stringLength: {
								min:10,
								max:10,
								message: 'must be 10 digits, including zipcode'
							},
							regexp: {
								regexp: /^([1-9]{1}[0-9]{9})*$/,
								message: 'must only contain numbers. must be 10 digits, including zipcode. Cannot start with a 0'
							
							},
						},
					},
					balance: {
							validators: {
							notEmpty: {
								message: 'balance required'
							},
							stringLength: {
								min:1,
								max:7,
								message: 'format $9999.99'
							},
							regexp: {
								//regexp: /^[0-9][0-9]\.{0,2}+$/,
								regexp: /^([0-9])+(\.?)([0-9][0-9])+$/,
								message: 'balance is required'
							},
						},
					},
					sales: {
							validators: {
							notEmpty: {
								message: 'total sales required'
							},
							stringLength: {
								min:1,
								max:7,
								message: 'format $9999.99'
							},
							regexp: {
								//regexp: /^[0-9][0-9]\.{0,2}+$/,
								regexp: /^([0-9])+(\.?)([0-9][0-9])+$/,
								message: 'total sales is required'
							},
						},
					},
					notes: {
							validators: {
							
							stringLength: {
								min:1,
								max:100,
								message: '100 character max!'
							},
							regexp: {
								//regexp: /^([A-Za-z0-9\,\. _)*$/,
								regexp: /^[a-zA-Z0-9~@#\^\$&\*\(\)%<>-_\+=\[\]\{\}\|\\,\.\?\!\s]*$/,
								message: 'optional notes!:)'
							},
						},
					},		
			}
	});
});
</script>

</body>
</html>
