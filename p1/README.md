> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Web Applications Development

## Daniel Rigby

### Project #1 Requirements:

1. Successfully clone student files
2. Modify Meta tags
3. Provide form validation 


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A3;
* Screenshot of ERD;




#### Assignment Screenshots:

*Screenshot of main page:*

![Main](img/main.PNG)

*Screenshot of form validation:*

![Valid](img/validation.PNG)

*Screenshot of invalid:*

![Invalid](img/nvalid.PNG)



#### ERD Links:

*Home page *
[link](http://localhost:9999/lis4368 "link")

