> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Mobile Web App Development

## Daniel Rigby

### Assignment #1 Requirements:


#### README.md file should include the following items:

1. Distributed version control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo


>
> #### Git commands w/short descriptions:

1. git init - Creates a new git repository
2. git status - lets you see which changes have been staged, which haven’t, and which files aren’t being tracked by Git.
3. git add - adds a change in the working area to the staging area.
4. git commit - commits any changes in the staging area to the projects history.
5. git push -  transfer commits from your local repository to a remote repo.
6. git pull - gets the specified remote’s copy of the current branch and immediately merge it into the local copy.
7. git revert - undoes the last commit made to the project, however instead of removing it entirely it keeps it as reference.


#### Assignment Screenshots:

*Screenshot of tomcat running http://localhost*:9999

![Tomcat](img/Capture.PNG)

*Screenshot of running java Hello*:

![Hello](img/Capture1.PNG)


