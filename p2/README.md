> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Web Applications Development

## Daniel Rigby

### Project #2 Requirements:

	- MVC framework, using basic client-, server-side validation
	- Prepared statements to help prevent SQL injection.
	- JSTL to prevent XSS. Also, completes CRUD functionality. 


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per P2;
* Screenshot of ERD;




#### Assignment Screenshots:

*Screenshot of main page:*

![Main](img/form.PNG)

*Screenshot of form validation:*

![mod](img/mod.PNG)

*Screenshot of thanks:*

![thanks](thanks/.PNG)

*Screenshot of data:*

![Invalid](data/.PNG)

*Screenshot of modded:*

![Invalid](modded/.PNG)

*Screenshot of sql:*

![Invalid](sql/.PNG)



#### ERD Links:

*Home page *
[link](http://localhost:9999/lis4368 "link")

