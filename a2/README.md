> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Mobile Web App Development

## Daniel Rigby

### Assignment #2 Requirements:


#### README.md file should include the following items:

1. Download and install Mysql
2. Create new user
3. Develop servlet
4. Deploy servlet




#### Assignment Screenshots:

*Directory

![Directory](img/directory.PNG)

*Home

![Home](img/home.PNG)

*QueryBook

![QueryBook](img/querybook.PNG)

*Results

![Results](img/queryresults.PNG)

*SayHello

![Sayhello](img/sayhello.PNG)

*SayHi

![SayHi](img/sayhi.PNG)