> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Web Applications Development

## Daniel Rigby

### Assignment #3 Requirements:

1. Create an ERD of the pet store database
2. Create a petstore DB 


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A3;
* Screenshot of ERD;
* Links to the following files:
	* a3.mwb
	* a3.sql



#### Assignment Screenshots:

*Screenshot of pet store ERD:*

![PetStore ERD](img/a3.png)




#### ERD Links:

*PetStore MWB link:*
[A3 PetStore MWB link](database/a3.mwb "PetStore MWB")

*PetStore SQL link:*
[A3 PetStore SQL link](database/a3.sql "PetStore SQL")
