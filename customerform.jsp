<%-- Use core library --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates server and client side validation.">
	<meta name="author" content="Daniel Rigby">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - JSP Forms</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->	
	
	<%@ include file="/global/nav_global.jsp" %>	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="page-header">

					<!-- View source or uncomment to display Parameter value...
								Parameter value: <%= request.getParameter("assign_num") %>

								Note: use JSTL (JSP Standard Tag Library) combined with EL (Expression Language), instead of Java scriplets to run conditional logic.
								To use core tag must include tag Library link in first line of file - see top.
								<%  //@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
								Also, need taglib .jar files. See WEB-INF > lib directory.
						-->

					<%  // w/o using taglib: String anum = request.getParameter("assign_num"); %>
					<!-- Or, using JSTL's expression language (EL): request parameters made available in implicit param object. -->
						
					<c:set var="anum" value="${param.assign_num}" scope="request" />
					<!-- Uncomment to Print: -->
					<%-- <c:out value="${anum}" /> --%>
					<c:choose>
						<c:when test="${anum == null || anum == '0'}">
						<%@ include file="/global/header.jsp" %>
					</div>
					<p><i>${message}</i></p>
						</c:when>

						<c:when test="${anum == 'a4'}">
							<%@ include file="/a4/global/header.jsp" %>													
				</div>
						</c:when>

						<c:when test="${anum == 'a5'}">
							<%@ include file="/a5/global/header.jsp" %>
			</div>
						</c:when>

						<c:when test="${anum == 'p2'}">
							<%@ include file="/p2/global/header.jsp" %>													
			</div>
						</c:when>
						
						<c:otherwise>
							<% response.sendRedirect("/lis4368/index.jsp"); %>
						</c:otherwise>
					</c:choose>
					
<!--
https://www.tutorialspoint.com/servlets/servlets-first-example.htm								
http://stackoverflow.com/questions/11731377/servlet-returns-http-status-404-the-requested-resource-servlet-is-not-availa								
Form action submission to URL with leading slash:
<form action="/servlet">
Leading slash / makes URL relative to domain, that is, form will submit to:
http://localhost:9999/servlet

Form action submission to URL w/o leading slash:								
<form action="servlet">
Makes URL relative to current directory of current URL. Form will submit to:
http://localhost:9999/contextname/somedirectory/servlet								

Best solution: make URL domain-relative (won't need to fix URLs when JSP/HTML files moved into another folder)
<form action="${pageContext.request.contextPath}/servlet">

Makes URL context relative. Form will submit to:								
<form action="/contextname/servlet">
Will *always* submit to correct URL!
-->
					<p><a href="customerAdmin">Display Customers</a></p>

					<form id="add_customer_form" method="post" class="form-horizontal" action="${pageContext.request.contextPath}/customerAdmin">					
						
						<input type="hidden" name="add_customer" value="add">						
						
						<div class="form-group">
							<label class="col-sm-3 control-label">First Name:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="fname" maxlength="15" value="${customer.fname}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Last Name:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="lname" maxlength="30" value="${customer.lname}" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Email:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" maxlength="100" value="${customer.email}" />
							</div>
						</div>
						<div class="form-group">
						        <label class="col-sm-3 control-label">Street:</label>
							<div class="col-sm-5">
							     <input type="text" class="form-control" name="street" maxlength="30" value="${customer.street}" />
							</div>     
						</div>
						<div class="form-group">
						        <label class="col-sm-3 control-label">City:</label>
							<div class="col-sm-5">
							     <input type="text" class="form-control" name="city" maxlength="30" value="${customer.city}" />
							</div>  
						</div>  
						
						<div class="form-group">
						        <label class="col-sm-3 control-label">State:</label>
							<div class="col-sm-5">
							    <input type="text" class="form-control" name="state" maxlength="2" value="${customer.state}" />
							</div>
						</div>
						<div class="form-group">
						       <label class="col-sm-3 control-label">Zip:</label>
						       <div class="col-sm-5">
							    <input type="text" class="form-control" name="zipcode" maxlength="9" value="${customer.zipcode}" />
						       </div>	    
						</div>
						<div class="form-group">
						       <label class="col-sm-3 control-label">Phone:</label>
						       <div class="col-sm-5">
							   <input type="text" class="form-control" name="phone" maxlength="10" value="${customer.phone}" />
						       </div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Balance:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="balance" maxlength="7" value="${customer.balance}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Sales:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="sales" maxlength="7" value="${customer.sales}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Notes:</label>
							<div class="col-sm-5">
							<input type="text" class="form-control" name="notes" maxlength="255" value="${customer.notes}" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%--@ include file="/js/include_js.jsp" --%>		
 
<script type="text/javascript">
$(document).ready(function() {

	$('#add_customer_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {

				fname: {
							validators: {
									notEmpty: {
											message: 'First name required'
									},
									stringLength: {
											min: 1,
											max: 15,
											message: 'First name no more than 15 characters'
									},
									regexp: {
										//http://www.regular-expressions.info/
										//http://www.regular-expressions.info/quickstart.html
										//http://www.regular-expressions.info/shorthand.html
										//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
										//alphanumeric (also, "+" prevents empty strings):
										regexp: /^[a-zA-Z\-]+$/,
										message: 'First name can only contain letters and hyphens.'
									},									
							},
					},

				lname: {
							validators: {
									notEmpty: {
											message: 'Last name required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Last name no more than 30 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z\-]+$/,
										message: 'Last name can only contain letters and hyphens'
									},									
							},
					},				
					
					email: {
							validators: {
									notEmpty: {
											message: 'Email address is required'
									},

									/*
									//built-in e-mail validator, comes with formValidation.min.js
									//using regexp instead (below)
									emailAddress: {
											message: 'Must include valid email address'
									},
									*/
								
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
									regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
										message: 'Must include valid email'
									},																		
							},
					},
					street: {
							validators: {
									notEmpty: {
											message: 'street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'max 30 characters'
									},
									regexp: {
										//alphanumeric (also, "+" prevents empty strings):
										regexp: /^([A-Za-z0-9\-\s\.]+$/,
										message: 'may contain only letters, numbers, periods, commas and white spaces'
									},									
							},
					},
					city: {
							validators: {
									notEmpty: {
											message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'max 30 characters'
									},
									regexp: {
										regexp: /^([A-Za-z0-9\,\. _]*[A-Za-z0-9][A-Za-z0-9\,\. _])*$/,
										message: 'can contain only letters and numbers'
									},									
							},
					},
					state: {
							validators: {
									notEmpty: {
											message: 'State required'
									},
									stringLength: {
											min: 1,
											max: 2,
											message: 'max 2 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z\-]+$/,
										message: 'in "FL" format. Only letters'
									},									
							},
					},
					zipcode: {
						validators: {
									notEmpty: {
											message: 'zipcode required'
									},
									stringLength: {
											min: 5,
											max: 9,
											message: 'min 5 digits, max 9 digits'
									},
									regexp: {
										regexp: /^([0-9])+$/,
										message: 'only numbers accepted'
									},									
							},
					},
					phone: {
						validators: {
							notEmpty: {
								message: 'phone number required'
							},
							stringLength: {
								min:10,
								max:10,
								message: 'must be 10 digits, including zipcode'
							},
							regexp: {
								regexp: /^([1-9]{1}[0-9]{9})*$/,
								message: 'must only contain numbers. must be 10 digits, including zipcode. Cannot start with a 0'
							
							},
						},
					},
					balance: {
							validators: {
							notEmpty: {
								message: 'balance required'
							},
							stringLength: {
								min:1,
								max:7,
								message: 'format $9999.99'
							},
							regexp: {
								//regexp: /^[0-9][0-9]\.{0,2}+$/,
								regexp: /^([0-9])+(\.?)([0-9][0-9])+$/,
								message: 'balance is required'
							},
						},
					},
					sales: {
							validators: {
							notEmpty: {
								message: 'total sales required'
							},
							stringLength: {
								min:1,
								max:7,
								message: 'format $9999.99'
							},
							regexp: {
								//regexp: /^[0-9][0-9]\.{0,2}+$/,
								regexp: /^([0-9])+(\.?)([0-9][0-9])+$/,
								message: 'total sales is required'
							},
						},
					},
					notes: {
							validators: {
							
							stringLength: {
								min:1,
								max:100,
								message: '100 character max!'
							},
							regexp: {
								//regexp: /^([A-Za-z0-9\,\. _)*$/,
								regexp: /^[a-zA-Z0-9~@#\^\$&\*\(\)%<>-_\+=\[\]\{\}\|\\,\.\?\!\s]*$/,
								message: 'optional notes!:)'
							},
						},
					},		
			}
	});
});
</script>

</body>
</html>
