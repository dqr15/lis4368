> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Web Applications Development

## Daniel Rigby

### Assignment #5 Requirements:

1. cd to webapps subdirectory:
	Example (windows): cd C:/tomcat/webapps
2. Clone assignment starter files:
	git clone https://bitbucket.org/mjowett/lis4368_student_files
	(clones lis4368_student_files Bitbucket repo)
	Note: If there is an existing lis4368_student_files, it *will* need to be renamed—otherwise, error!
	a. Review subdirectories and files, especially META-INF and WEB-INF
	b. Each assignment *must* have its own global subdirectory.
3. NB: Because of the way the newer version of Tomcat recognizes web applications (as well as
	associated include files), *all* of the assignments will need to be placed under lis4368. 


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A5;
* Screenshot of form;
* Screenshot of completed form
* Screenshot of submission



#### Assignment Screenshots:

*Screenshot of form:*

![form](img/first.PNG)

*Screenshot of data:*

![data](img/second.PNG)

*Screenshot of thanks:*

![thanks](img/third.PNG)




